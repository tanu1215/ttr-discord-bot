from abc import ABC, abstractmethod
from enum import Enum
import discord
from utils.JsonReader import JsonReader
import asyncio


class Permission(Enum):
    Everyone = 0,
    Moderator = 1,
    Administrator = 2


# client = discord.Client()
# discord.Member
# discord.User
# discord.Channel
# discord.Role

class Command(ABC):
    def __init__(self, command_name: str, bot: discord.Client = None, permission: Permission = None):
        self.command_name = command_name
        if bot is not None:
            self.bot = bot
        if permission is not None:
            self.permission = permission

    async def register(self, message: discord.Message):
        if message.channel.name != "bot-testing":
            return

        if message.author == self.bot.user:
            return

        if not message.content.startswith(self.command_name):
            return

        can_pass = False

        if self.permission is None:
            self.permission = Permission.Everyone

        elif self.permission is Permission.Moderator:
            for role in message.author.roles:
                if role.name == "Resistance Recruits":
                    can_pass = True

            if not can_pass:
                print("Author doesn't have permission for moderator")
                return

        elif self.permission is Permission.Administrator:
            for role in message.author.roles:
                if role.name == "Toon Resistance":
                    can_pass = True

            if not can_pass:
                print("Author doesn't have permission for administrator")
                return

        await self.register_command(message)

        print("Command sent by " + message.author.name)
        print(" ")

        for role in message.author.roles:
            print(role)

    @abstractmethod
    async def register_command(self, message: discord.Message):
        pass

    def to_string(self):
        return self.command_name


class JokeCommand(Command):
    def __init__(self, command_name: str, bot, permission=None):
        super().__init__(command_name=command_name, bot=bot, permission=permission)
        self.bot = bot

    async def register_command(self, message: discord.Message):
        await self.bot.send_message(message.channel, "What goes TICK-TICK-TICK-WOOF? A watchdog!")


class CloseCommand(Command):
    def __init__(self, command_name: str, bot, permission=None):
        super().__init__(command_name=command_name, bot=bot, permission=permission)
        self.bot = bot

    async def register_command(self, message: discord.Message):
        await self.bot.logout()


class InvasionsCommand(Command):
    def __init__(self, command_name: str, bot, permission=None):
        super().__init__(command_name=command_name, bot=bot, permission=permission)
        self.bot = bot
        self.json = JsonReader("http://toonhq.org/api/v1/invasion/")

    async def register_command(self, message: discord.Message):
        json_requests = self.json.get_requests()

        await self.bot.send_message(message.channel, "Invasions:")

        for index in json_requests["invasions"]:
            await self.bot.send_message(message.channel, "There is a " + index["cog"] + " invasion in " +
                                        index["district"])
            await asyncio.sleep(1000)


class NewsCommand(Command):
    def __init__(self, command_name: str, bot, permission=None):
        super().__init__(command_name=command_name, bot=bot, permission=permission)
        self.bot = bot
        self.json = JsonReader("https://www.toontownrewritten.com/api/news")

    async def register_command(self, message: discord.Message):
        json_requests = self.json.get_requests()

        await self.bot.send_message(message.channel, "Latest news:")
        await self.bot.send_message(message.channel, json_requests["title"] + " Author: " + json_requests["author"] +
                                    " Date: " + json_requests["date"])
        await self.bot.send_message(message.channel, "https://www.toontownrewritten.com/news")
        await asyncio.sleep(1000)

