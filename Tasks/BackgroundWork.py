import discord
import asyncio
from utils.JsonReader import JsonReader
from utils.Twitter import Twitter


class AutomaticNewsReciever:
    def __init__(self, bot: discord.Client()):
        self.bot = bot
        self.json = JsonReader("https://www.toontownrewritten.com/api/news")

    async def start_news_loop(self):
        await self.bot.wait_until_ready()
        channel = discord.Object(id=300509338018250753)

        json_requests = self.json.get_requests()
        post_id = json_requests["postId"]

        while not self.bot.is_closed:
            if not json_requests["postId"] == post_id:
                post_id = json_requests["postId"]

                await self.print_news(channel, json_requests)

            await asyncio.sleep(21600)  # task runs every 6 hours

    async def print_news(self, channel, json_requests):
        await self.bot.send_message(channel, "There's new news!")
        await self.bot.send_message(channel,
                                    json_requests["title"] + " Author: " + json_requests["author"] +
                                    ". Date: " + json_requests["date"] + ".")
        await self.bot.send_message(channel, "https://www.toontownrewritten.com/news")


class AutomaticTwitterReader:
    def __init__(self, bot: discord.Client()):
        self.bot = bot
        self.twitter = Twitter("dWwkqlA9B1jVWg6Xu2td8Wchv",
                               "WvbvMcfBMka2Wy1HpHGPXucVVWP1fg5plnASlZYwQ1EcVpLxtI",
                               "835584779634405376-5RCMPSLorPwBxnj6GStAj7f7NlVsLmF",
                               "0oBpiyEkDNdm8ptr2VpspQKle1UDKRNucHeEWVDx7bVkO")

    async def start_twitter_loop(self):
        await self.bot.wait_until_ready()
        channel = discord.Object(303260438966304770)

        while not self.bot.is_closed:
            self.twitter.read_twitter()

        await asyncio.sleep(5)
