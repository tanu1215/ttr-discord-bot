class FileReader:
    def __init__(self, file_path: str, read_write_command):
        self.file_path = file_path

    def Open(self, read_or_write):
        if read_or_write == 'w':
            self.fo = open(self.file_path, read_or_write)
        elif read_or_write == 'r':
            self.fo = open(self.file_path, read_or_write)
        else:
            raise ValueError("Cannot do anything other than read or write the file, " + self.file_path + "!")

    def Close(self):
        self.fo.close()

    def GetContents(self):
        return [x for x in self.fo.read().split(',')]
