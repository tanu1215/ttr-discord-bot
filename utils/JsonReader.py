import requests

class JsonReader:
    def __init__(self, url):
        self.url = url

    def get_requests(self):
        return requests.get(self.url).json()
