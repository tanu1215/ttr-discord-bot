import tweepy
import json


class Twitter:
    def __init__(self, consumer_key, consumer_secret, access_key, access_secret):
        self.auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        self.__get_auth_url()
        self.auth.set_access_token(access_key, access_secret)
        self.api = tweepy.API(self.auth)

    def read_twitter(self):
        stream = tweepy.Stream(self.auth, 1)
        stream.filter(track="2733200366")
    def __get_auth_url(self):
        try:
            self.auth.get_authorization_url()
        except tweepy.TweepError:
            print("There was an error getting the twitter authentication")

    class Stream(tweepy.StreamListener):
        def on_data(self, data):
            # Twitter returns data in JSON format - we need to decode it first
            decoded = json.loads(data)

            # Also, we convert UTF-8 to ASCII ignoring all bad characters sent by users
            print('@%s: %s' % (decoded['user']['screen_name'], decoded['text'].encode('ascii', 'ignore')))
            print(" ")
            return True

        def on_error(self, status):
            print(status)



