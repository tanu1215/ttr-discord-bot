import discord
from Commands.Command import Command, JokeCommand, CloseCommand, InvasionsCommand, NewsCommand
from Commands.Command import Permission
from Tasks import BackgroundWork
import asyncio

client = discord.Client()
commands = [JokeCommand("!joke", bot=client, permission=Permission.Everyone),
            CloseCommand("!close", bot=client, permission=Permission.Moderator),
            InvasionsCommand("!invasions", bot=client, permission=Permission.Everyone),
            NewsCommand("!news", bot=client, permission=Permission.Everyone)]


@client.async_event
def on_ready():
    print("")


# @client.async_event
# async def on_member_join(member: discord.Member):
   # server = member.server
   # fmt = "Welcome to {0.name}, {1.mention}! Please check out the rules and remember to stay toony!"
   # await client.send_message(server, fmt.format(server, member))
   # await client.add_roles(member, discord.Role(name="Toons"))


# @client.async_event
# async def on_member_remove(member: discord.Member):
   # server = member.server
   # fmt = "**{0.name}** was hit by an anvil!"
   # await client.send_message(server, fmt.format(member))


@client.async_event
async def on_message(message: discord.Message):
    for command in commands:
        await command.register(message)

client.loop.create_task(BackgroundWork.AutomaticNewsReciever(client).start_news_loop())
client.loop.create_task(BackgroundWork.AutomaticTwitterReader(client).start_twitter_loop())
client.run("MzAxODAwOTcwMDA0NzI1NzYw.C9GYxA.7fC64RYIUahJjAluNFNJgQYNY0s")
